
__requires__ = ['SQLAlchemy >= 0.7']
import pkg_resources

import unittest
import json
import sys
import os
import hashlib


from datetime import date
from datetime import timedelta

from contextlib import contextmanager
from flask import appcontext_pushed, g

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.orm import scoped_session

sys.path.insert(0, os.path.join(os.path.dirname(
    os.path.abspath(__file__)), '..'))

from bugspad2 import APP, login
import bugspad2.lib as mmlib
from bugspad2.lib import model

#DB_PATH = 'sqlite:///:memory:'
## A file database is required to check the integrity, don't ask
DB_PATH = 'sqlite:////tmp/test.sqlite'



class FakeUser(object):
    """ Fake FAS user used for the tests. """
    id = 1
    user_name = 'kdas'
    cla_done = True
    groups = ['admin',]
    email_address = 'kushaldas@gmail.com'


@contextmanager
def user_set(APP, user):
    """ Set the provided user as fas_user in the provided application."""

    # Hack used to remove the before_request function set by
    # flask.ext.fas_openid.FAS which otherwise kills our effort to set a
    # flask.g.fas_user.
    APP.before_request_funcs[None] = []

    def handler(sender, **kwargs):
        g.fas_user = user
        g.fas_session_id = 'asnsf'

    with appcontext_pushed.connected_to(handler, APP):
        yield


class Modeltests(unittest.TestCase):
    """ Model tests. """

    def __init__(self, method_name='runTest'):
        """ Constructor. """
        unittest.TestCase.__init__(self, method_name)
        self.session = None

    # pylint: disable=C0103
    def setUp(self):
        """ Set up the environnment, ran before every tests. """
        if '///' in DB_PATH:
            dbfile = DB_PATH.split('///')[1]
            if os.path.exists(dbfile):
                os.unlink(dbfile)
        self.session = model.create_tables(DB_PATH, debug=False)
        APP.before_request(login._check_session_cookie)

    # pylint: disable=C0103
    def tearDown(self):
        """ Remove the test.db database if there is one. """
        if '///' in DB_PATH:
            dbfile = DB_PATH.split('///')[1]
            if os.path.exists(dbfile):
                os.unlink(dbfile)

        self.session.rollback()
        self.session.close()
        mmlib.redis.flushdb()




def add_test_data(session):
    password = 'asdf%s' % APP.config.get('PASSWORD_SEED', None)
    password = hashlib.sha512(password).hexdigest()
    user = model.User(user_name='kdas',email_address='kushaldas@gmail.com',display_name='Kushal Das', password=password)
    session.add(user)
    session.commit()
    name = "Fedora"
    desc = "OS"
    pr = model.Product(name=name, description=desc)
    session.add(pr)
    session.commit()
    name = '20'
    ver = model.Version(name=name, product_id=pr.id, active=True)
    session.add(ver)
    session.commit()
    com = model.Component(name='Python', description='The language', product_id=1)
    session.add(com)
    session.commit()
    mmlib.load_all(session)
    #data = json.loads(mmlib.redis.get('product:%s' % pr.id))
    #data.append((ver.id, name))
    #mmlib.redis.set('product:%s' % str(pr.id), json.dumps(data))