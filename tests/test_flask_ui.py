
__requires__ = ['SQLAlchemy >= 0.7']
import pkg_resources

import json
import unittest
import sys
import os

from mock import patch

sys.path.insert(0, os.path.join(os.path.dirname(
    os.path.abspath(__file__)), '..'))

import bugspad2
from bugspad2.lib import model
from tests import (Modeltests, user_set, add_test_data, FakeUser)


class FlaskUiTest(Modeltests):
    """ Flask tests. """

    def setUp(self):
        """ Set up the environnment, ran before every tests. """
        super(FlaskUiTest, self).setUp()

        bugspad2.APP.config['TESTING'] = True
        bugspad2.lib.ISTEST = True
        bugspad2.SESSION = self.session
        add_test_data(self.session)
        self.app = bugspad2.APP.test_client()

    def login(self, username, password):
        return self.app.post('/dologin', data=dict(
            username=username,
            password=password
        ), follow_redirects=True)

    def logout(self):
        return self.app.get('/logout', follow_redirects=True)


    def test_homepage(self):
        rv = self.app.get('/')
        self.assertTrue('login' in rv.data)

    @patch('bugspad2.login_required')
    def test_product_page(self, login_func):
        login_func.return_value = None
        user = FakeUser()
        with user_set(bugspad2.APP, user):
            output = self.app.get('/products/')
            self.assertTrue('Fedora' in output.data)

    @patch('bugspad2.login_required')
    def test_filebug_page(self, login_func):
        login_func.return_value = None
        user = FakeUser()
        with user_set(bugspad2.APP, user):
            self.file_a_bug()

    @patch('bugspad2.login_required')
    def test_updatebug_page(self, login_func):
        login_func.return_value = None
        user = FakeUser()
        with user_set(bugspad2.APP, user):
            op = self.file_a_bug()
            self.update_a_bug(op)

    def update_a_bug(self, output):
        csrf_token = output.data.split(
            'name="csrf_token" type="hidden" value="')[1].split('">')[0]
        data = {
            'summary': 'New bug in house.',
            'bug_status': 'closed',
            'bug_severity': 'critical',
            'bug_component': '1',
            'bug_versions': '1',
            'bug_priority': 'medium',
            'bug_assignee': 'kdas',
            'csrf_token': csrf_token,
            'upload': '',
            'new_comment': 'My first comment.',
            'bug_blocks_on': '',
            'bug_depends_on': '',
            'bug_cc': '',
            'bug_docs': '',
            'bug_hardware': '',
            'bug_whiteboard': '',
            'product_id': '1'
        }
        output = self.app.post('/bugs/1', data=data,
                               follow_redirects=True, content_type='multipart/form-data')

        self.assertTrue('''<option value="critical" selected>Critical</option>'''
                       in output.data)
        self.assertTrue('''<option value="closed" selected>Closed</option>'''
                        in output.data)
        self.assertTrue('''<pre>UPDATE:
Status updated from new to closed.

Severity updated from normal to critical.
</pre>''' in output.data)
        self.assertTrue('''<pre>My first comment.</pre>''' in output.data)


    def file_a_bug(self):
        output = self.app.get('/bugs/new/1')
        self.assertTrue('20' in output.data)

        csrf_token = output.data.split(
            'name="csrf_token" type="hidden" value="')[1].split('">')[0]

        data = {
            'summary': 'New bug in house.',
            'bug_desc': 'The description of the bug.',
            'bug_status': 'new',
            'bug_severity': 'normal',
            'bug_component': '1',
            'bug_versions': '1',
            'bug_priority': 'medium',
            'bug_assignee': 'kdas',
            'csrf_token': csrf_token,
            'product_id': '1'
        }
        output = self.app.post('/bugs/new/', data=data,
                               follow_redirects=True)
        self.assertTrue('<h3>New bug in house.</h3>' in output.data)
        return output



if __name__ == '__main__':
    SUITE = unittest.TestLoader().loadTestsFromTestCase(FlaskUiTest)
    unittest.TextTestRunner(verbosity=2).run(SUITE)