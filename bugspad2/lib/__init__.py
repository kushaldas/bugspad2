# -*- coding: utf-8 -*-
#
# Copyright © 2014  Kushal Das <kushaldas@gmail.com>
# Copyright © 2014  Red Hat, Inc.
#
# This copyrighted material is made available to anyone wishing to use,
# modify, copy, or redistribute it subject to the terms and conditions
# of the GNU General Public License v.2, or (at your option) any later
# version.  This program is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY expressed or implied, including the
# implied warranties of MERCHANTABILITY or FITNESS FOR A PARTICULAR
# PURPOSE.  See the GNU General Public License for more details.  You
# should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#

'''
Bugspad internal api.
'''
__requires__ = ['SQLAlchemy >= 0.7', 'jinja2 >= 2.4']
import pkg_resources

import datetime
import random
import string
import json
import hashlib
import logging
from datetime import datetime
from pprint import pprint
from collections import OrderedDict

from redis import Redis
redis = Redis()

import os
import sqlalchemy

from sqlalchemy.orm import sessionmaker
from sqlalchemy.orm import scoped_session
from sqlalchemy.orm.exc import NoResultFound
from sqlalchemy.exc import SQLAlchemyError
from werkzeug.utils import secure_filename


from bugspad2.lib import model
from bugspad2.lib import notifications
from bugspad2 import default_config

ISTEST = False #to mute all prints while in tests.

logger = logging.getLogger(__name__)


def create_session(db_url, debug=False, pool_recycle=3600):
    ''' Create the Session object to use to query the database.

    :arg db_url: URL used to connect to the database. The URL contains
    information with regards to the database engine, the host to connect
    to, the user and password and the database name.
      ie: <engine>://<user>:<password>@<host>/<dbname>
    :kwarg debug: a boolean specifying wether we should have the verbose
        output of sqlalchemy or not.
    :return a Session that can be used to query the database.

    '''
    engine = sqlalchemy.create_engine(
        db_url, echo=debug, pool_recycle=pool_recycle)
    scopedsession = scoped_session(sessionmaker(bind=engine))
    return scopedsession

def get_user_by_username(session, username):
    ''' Return a specified User via its username.

    :arg session: the session with which to connect to the database.

    '''
    query = session.query(
        model.User
    ).filter(
        model.User.user_name == username
    )

    return query.first()


def get_user_by_email(session, email):
    ''' Return a specified User via its email address.

    :arg session: the session with which to connect to the database.

    '''
    query = session.query(
        model.User
    ).filter(
        model.User.email_address == email
    )

    return query.first()


def get_user_by_token(session, token):
    ''' Return a specified User via its token.

    :arg session: the session with which to connect to the database.

    '''
    query = session.query(
        model.User
    ).filter(
        model.User.token == token
    )

    return query.first()

def get_user_by_losttoken(session, token):
    ''' Return a specified User via its token.

    :arg session: the session with which to connect to the database.

    '''
    query = session.query(
        model.User
    ).filter(
        model.User.losttoken == token
    )

    return query.first()


def get_session_by_visitkey(session, sessionid):
    ''' Return a specified VisitUser via its session identifier (visit_key).

    :arg session: the session with which to connect to the database.

    '''
    query = session.query(
        model.UserVisit
    ).filter(
        model.UserVisit.visit_key == sessionid
    )

    return query.first()

def id_generator(size=15, chars=string.ascii_uppercase + string.digits):
    """ Generates a random identifier for the given size and using the
    specified characters.
    If no size is specified, it uses 15 as default.
    If no characters are specified, it uses ascii char upper case and
    digits.
    :arg size: the size of the identifier to return.
    :arg chars: the list of characters that can be used in the
        idenfitier.
    """
    return ''.join(random.choice(chars) for x in range(size))


def load_all(session):
    '''Load all details as required into redis

    :arg session: the session with which to connect to the database.
    '''
    query = session.query(model.User)
    for row in query:
        redis.hset("users", row.user_name, row.id)
        redis.hset("usersid", row.id, row.user_name)
        redis.hset('userpass', row.user_name, row.password)
    if not ISTEST:
        print "Users loaded in redis."

    productlist = []
    query = session.query(model.Product)
    for row in query:
        result = []
        for ver in row.versions:
            if ver.active == True:
                result.append((ver.id, ver.name))
        data = json.dumps(result)
        redis.set("product:%s" % str(row.id), data)
        productlist.append((row.id, row.name))
        query2 = session.query(model.Component).filter(model.Component.product_id==row.id)
        component_list = []
        for row2 in query2:
            component_list.append((row2.id, row2.name, row2.description))
            redis.hset('componenthash',str(row2.id), row2.name ) # For id->name in components.
        redis.set('componentlist:%s' % str(row.id), json.dumps(component_list))

    redis.set('productlist', json.dumps(productlist))
    if not ISTEST:
        print "Products are loaded in memory."


def get_products():
    "Returns the list of the products from redis."
    try:
        return json.loads(redis.get('productlist'))
    except Exception:
        #TODO: Log the error
        pass
    return []

def get_versions(product_id):
    "Returns the list of id,versions for a given product."
    data = json.loads(redis.get('product:%s' % str(product_id)))
    return [(unicode(word[0]), unicode(word[1])) for word in data] #unicode is required for form validation.


def get_components(product_id):
    'Returns the list of id,names for a given product.'
    data = json.loads(redis.get('componentlist:%s' % str(product_id)))
    return [(unicode(word[0]), unicode(word[1])) for word in data] #unicode is required for form validation.

def load_bugs(session):
    'Loads the bugs into redis.'
    query = session.query(model.Bugs)
    for row in query:
        bug = {}
        bug['component'] = unicode(row.component_id)
        bug['bug_status'] = row.status
        bug['severity'] = row.severity
        bug['priority'] = row.priority
        bug['bug_whiteboard'] = row.whiteboard
        bug['bug_versions'] = unicode(row.version)
        bug['bug_hardware'] = row.hardware
        bug['bug_assignee'] = row.assigned_to if row.assigned_to else ''
        bug['bug_depends_on'] = row.depends_on
        bug['bug_blocks_on'] = row.blocks_on
        bug['bug_cc'] = row.cc
        bug['bug_docs'] = row.docs
        bug['id'] = row.id
        bug['summary'] = row.summary
        bug['desc'] = row.description
        bug['product_id'] = unicode(row.product_id)
        bug['reporter'] = unicode(get_user_name(row.reporter))
        bug_save_redis(bug)
        save_assignee_list_redis(row.id, row.assigned_to)
        save_cc_list_redis(row.id, row.cc)
        save_reporter_redis(row.id, row.reporter)
    if not ISTEST:
        print "All bugs loaded."

    query = session.query(model.Uploads)
    for row in query:
        redis.hset('uploads', row.id, row.path)
    if not ISTEST:
        print "Uploaded files loaded."


def update_bug_form(session, form, bug_id, user_id, file=None, upload_path='./uploads'):
    '''Updates the bug.

    :arg session: Session object for database.
    :arg form: flask.request.form dict.
    :arg bug_id: ID of the bug.
    :arg user_id: ID of the user
    '''
    bugd = {}
    bugd['component'] = unicode(form['bug_component']).strip()
    bugd['severity'] = unicode(form['bug_severity']).strip()
    bugd['priority'] = unicode(form['bug_priority']).strip()
    bugd['bug_status'] = unicode(form['bug_status']).strip()
    bugd['bug_whiteboard'] = unicode(form['bug_whiteboard']).strip()
    bugd['bug_versions'] = unicode(form['bug_versions']).strip()
    bugd['bug_hardware'] = unicode(form['bug_hardware']).strip()
    bugd['bug_assignee'] = unicode(form['bug_assignee']).strip()
    bugd['bug_depends_on'] = unicode(form['bug_depends_on']).strip()
    bugd['bug_blocks_on'] = unicode(form['bug_blocks_on']).strip()
    bugd['bug_cc'] = unicode(form['bug_cc']).strip()
    bugd['bug_docs'] = unicode(form['bug_docs']).strip()
    bugd['id'] = bug_id
    # If there is a file to upload we save it first.
    if file:
        filename = secure_filename(file.filename)
        filename = '%s_%s_%s' % (str(bug_id),datetime.now().strftime('%Y_%m_%d_%H_%M'), filename)
        file.save(os.path.join(upload_path, filename))
        up = model.Uploads(bug_id=bug_id,uploader=user_id,path=filename,uploaded=datetime.now())
        try:
            session.add(up)
            session.commit()
        except Exception:
            logger.error("Error in file upload.")
        redis.hset('uploads', up.id, filename)
    return update_bug(session, bugd, bug_id, user_id)

def update_bug(session, bug, bug_id, user_id):
    msg = u'UPDATE:'
    bugdb = session.query(model.Bugs).filter(model.Bugs.id==bug_id).first()
    bugdb.priority = bug['priority']
    bugdb.component_id = bug['component']
    bugdb.status = bug['bug_status']
    bugdb.severity = bug['severity']
    bugdb.whiteboard = bug['bug_whiteboard']
    bugdb.version = bug['bug_versions']
    bugdb.hardware = bug['bug_hardware']
    bug['bug_assignee'] = make_assignee_list(bug['bug_assignee'])
    bugdb.assignee = bug['bug_assignee']
    bugdb.depends_on = bug['bug_depends_on']
    bugdb.blocks_on = bug['bug_blocks_on']
    bug['bug_cc'] = make_assignee_list(bug['bug_cc']) # Get the unique list
    bugdb.cc = bug['bug_cc']
    bugdb.docs = bug['bug_docs']
    session.commit()
    obug = get_bug(bug_id)
    if obug['component'] != bug['component']:
        redis.hdel('bcomponent:' + obug['component'], str(bug_id))
        redis.hset('bcomponent:' + bug['component'], str(bug['id']), '1')
        msg = u'''%s
Component updated from %s to %s.
''' % (msg, get_component_name(obug['component']), get_component_name(bug['component']))
        obug['component'] = bug['component']

    if obug['bug_status'] != bug['bug_status']:
        redis.hdel('bstatus:' + obug['bug_status'], str(bug_id))
        redis.hset('bstatus:' + bug['bug_status'], str(bug['id']), '1')
        msg = u'''%s
Status updated from %s to %s.
''' % (msg, obug['bug_status'], bug['bug_status'])
        obug['bug_status'] = bug['bug_status']

    if obug['severity'] != bug['severity']:
        redis.hdel('bseverity:' + obug['severity'], str(bug_id))
        redis.hset('bseverity:' + bug['severity'], str(bug['id']), '1')
        msg = u'''%s
Severity updated from %s to %s.
''' % (msg, obug['severity'], bug['severity'])
        obug['severity'] = bug['severity']

    if obug['priority'] != bug['priority']:
        redis.hdel('bpriority:' + obug['priority'], str(bug_id))
        redis.hset('bpriority:' + bug['priority'], str(bug['id']), '1')
        msg = u'''%s
Priority updated from %s to %s.
''' % (msg, obug['priority'], bug['priority'])
        obug['priority'] = bug['priority']

    if obug['bug_versions'] != bug['bug_versions']:
        redis.hdel('bversion:' + obug['bug_versions'], str(bug_id))
        redis.hset('bversion:' + bug['bug_versions'], str(bug['id']), '1')
        msg = u'''%s
Version updated from %s to %s.
''' % (msg, obug['bug_version'], bug['bug_version'])
        obug['bug_version'] = bug['bug_version']

    if obug['bug_hardware'] != bug['bug_hardware']:
        msg = u'''%s
Hardware updated from %s to %s.
''' % (msg, obug['bug_hardware'], bug['bug_hardware'])
        obug['bug_hardware'] = bug['bug_hardware']

    if obug['bug_docs'] != bug['bug_docs']:
        msg = u'''%s
Docs updated from %s to %s.
''' % (msg, obug['bug_docs'], bug['bug_docs'])
        obug['bug_docs'] = bug['bug_docs']

    if obug['bug_assignee'] != bug['bug_assignee']:
        msg = u'''%s
Assignee updated from %s to %s.
''' % (msg, obug['bug_assignee'], bug['bug_assignee'])
        save_assignee_list_redis(bug_id, bug['bug_assignee'], obug['bug_assignee'])
        obug['bug_assignee'] = bug['bug_assignee']

    if obug['bug_cc'] != bug['bug_cc']:
        msg = u'''%s
CC updated from %s to %s.
''' % (msg, obug['bug_cc'], bug['bug_cc'])
        save_cc_list_redis(bug_id, bug['bug_cc'], obug['bug_cc'])
        obug['bug_cc'] = bug['bug_cc']

    # Remember to save the old bug object.
    redis.hset('bugs', str(bug_id), json.dumps(obug))
    # Now make a comment of the updates.
    if msg != u'UPDATE:':
        add_comment(session, bug_id, msg, user_id)


def save_bug_form(session, form, user_id):
    '''Saves the bug from a webform.

    :arg form: wtform instance.
    '''
    bugd = {}
    bugd['summary'] = form.summary.data
    bugd['desc'] = form.bug_desc.data
    bugd['component'] = form.bug_component.data
    bugd['severity'] = form.bug_severity.data
    bugd['priority'] = form.bug_priority.data
    bugd['bug_status'] = form.bug_status.data
    bugd['bug_whiteboard'] = form.bug_whiteboard.data
    bugd['bug_versions'] = form.bug_versions.data
    bugd['bug_hardware'] = form.bug_hardware.data
    bugd['bug_assignee'] = form.bug_assignee.data
    bugd['bug_depends_on'] = form.bug_depends_on.data
    bugd['bug_blocks_on'] = form.bug_blocks_on.data
    bugd['bug_cc'] = form.bug_cc.data
    bugd['bug_docs'] = form.bug_docs.data
    bugd['product_id'] = form.product_id.data

    return save_bug(session, bugd, user_id)

def save_bug(session, bug, user_id):
    'Saves the bug in database and redis.'
    newbug = model.Bugs()
    newbug.summary = bug['summary']
    newbug.priority = bug['priority']
    newbug.description = bug['desc']
    newbug.component_id = bug['component']
    newbug.status = bug['bug_status']
    newbug.severity = bug['severity']
    newbug.whiteboard = bug['bug_whiteboard']
    newbug.version = bug['bug_versions']
    newbug.hardware = bug['bug_hardware']
    bug['bug_assignee'] = make_assignee_list(bug['bug_assignee']) # Get the unique list.
    newbug.assignee = bug['bug_assignee']
    newbug.depends_on = bug['bug_depends_on']
    newbug.blocks_on = bug['bug_blocks_on']
    bug['bug_cc'] = make_assignee_list(bug['bug_cc']) # Get the unique list
    newbug.cc = bug['bug_cc']
    newbug.docs = bug['bug_docs']
    newbug.product_id = bug['product_id']
    newbug.reported = datetime.now()
    newbug.reporter = user_id

    #try:
    session.add(newbug)
    session.commit()

        #Now save in redis.
    bug['id'] = newbug.id
    bug['reporter'] = get_user_name(user_id) # To save the creator of the bug.
    bug_save_redis(bug)
    #except Exception, err:
    #    print err
    #    return err.message
    save_latest_created_bug(newbug.id)
    # Now save for the users.
    save_assignee_list_redis(newbug.id, bug['bug_assignee'])
    save_cc_list_redis(newbug.id, bug['bug_cc'])
    save_reporter_redis(newbug.id, user_id)
    return newbug.id

def bug_save_redis(bug):
    '''Saves the bug dict to redis.

    :arg bug: The dictionary
    '''
    redis.hset('bugs', str(bug['id']), json.dumps(bug))
    redis.hset('bcomponent:' + str(bug['component']), str(bug['id']), '1')
    redis.hset('bproduct:' + str(bug['product_id']), str(bug['id']), '1')
    redis.hset('bstatus:' + str(bug['bug_status']), str(bug['id']), '1')
    redis.hset('bpriority:' + bug['priority'], str(bug['id']), '1')
    redis.hset('bseverity:' + bug['severity'], str(bug['id']), '1')
    redis.hset('bversion:' + str(bug['bug_versions']), str(bug['id']), '1')

def save_latest_created_bug(bug_id):
    'Keeps the latest created list in redis.'
    redis.lpush('latest_created', unicode(bug_id))
    redis.ltrim('latest_created', 0, 10)

def save_latest_updated_bug(bug_id):
    'Keeps the latest updated list in redis.'
    redis.lpush('latest_updated', unicode(bug_id))
    redis.ltrim('latest_updated', 0, 10)

def get_latest_created_bug():
    'For index view'
    bug_ids = redis.lrange('latest_created', 0, -1)
    result = []
    for bid in bug_ids:
        result.append(get_bug(bid))
    return result

def get_latest_updated_bug():
    'For index view'
    bug_ids = OrderedDict([(x,x) for x in redis.lrange('latest_updated', 0, -1)])
    result = []
    for bid in bug_ids.keys():
        result.append(get_bug(bid))
    return result


def get_bug(bug_id):
    bug_id = str(bug_id)
    if redis.hexists('bugs', bug_id):
        data = redis.hget('bugs', bug_id)
        return json.loads(data)


def redis_auth(username, password):
    password = '%s%s' % (password, default_config.PASSWORD_SEED)
    password = hashlib.sha512(password).hexdigest()
    if redis.hexists('userpass', username):
        if unicode(password) == redis.hget('userpass', username):
            return redis.hget('users', username)
    return False


def add_comment(sesion, bug_id, comment, user_id):
    """

    :param sesion: Session object for database.
    :param bug_id: ID of the bug against which we are filling the comment.
    :param comment: The comment text in unicode.
    :param user_id: ID of the user who is making comment.

    :return: None or error message
    """
    com = model.Comments(bug_id=bug_id, mesg=comment, commenter=user_id,
                         reported=datetime.now())
    try:
        sesion.add(com)
        sesion.commit()
    except Exception, err:
        print err
        return err
    save_latest_updated_bug(bug_id)


def get_comments(session, bug_id):
    """

    :param session: Session object for database.
    :param bug_id: ID of the bug for which we want to view the comments.
    :return: A list of comments.
    """
    # Comments from database does not have reporter name, so we are adding it here.
    result = []
    for item in session.query(model.Comments).filter(model.Comments.bug_id==bug_id):
        item.reporter_name = get_user_name(item.commenter)
        result.append(item)
    return result


def get_uploads(session, bug_id):
    """

    :param session: Session object for database.
    :param bug_id: ID of the bug for which we want to view the uploads.
    :return: A list of files.
    """
    # Comments from database does not have uploader name, so we are adding it here.
    result = []
    for item in session.query(model.Uploads).filter(model.Uploads.bug_id==bug_id):
        item.uploader_name = get_user_name(item.uploader)
        result.append(item)
    return result


def get_component_name(component_id):
    '''

    :param component_id: ID of the component.
    :return: Name of the component.
    '''
    return redis.hget('componenthash', component_id)


def get_user_name(user_id):
    '''Returns the Username

    :param user_id: user ID
    :return: string containing the username.
    '''
    return redis.hget('usersid', user_id)


def make_assignee_list(ids=''):
    '''Returns a string, comma separated and sorted with valid usernames.

    :rtype : str
    :param ids: str with user names.
    :return: String comma separated and sorted.
    '''
    result = []
    for name in ids.split(','):
        name = name.strip()
        if redis.hexists('users', name):
            result.append(name)
    return ','.join(list(set(result))) # only the unique names


def save_assignee_list_redis(bug_id, ids='',oids=''):
    '''Saves the assignee list in redis.

    :param bug_id: ID of the bug.
    :param ids: String with new user names.
    :param oids: String with old user names.
    '''
    if oids is None:
        oids = ''
    if ids is None:
        ids = ''
    bug_id = str(bug_id)
    for name in oids.split(','):
        if name == '':
            continue
        redis.hdel('ass:%s' % name, bug_id) # First we delete all ids.
    for name in ids.split(','):
        if name == '':
            continue
        redis.hset('ass:%s' % name, bug_id, 1) # Then we recreate all ids.


def save_cc_list_redis(bug_id, ids='',oids=''):
    '''Saves the cc list in redis.

    :param bug_id: ID of the bug.
    :param ids: String with new user names.
    :param oids: String with old user names.
    '''
    if oids is None:
        oids = ''
    if ids is None:
        ids = ''
    bug_id = str(bug_id)
    for name in oids.split(','):
        if name == '':
            continue
        redis.hdel('cc:%s' % name, bug_id) # First we delete all ids.
    for name in ids.split(','):
        if name == '':
            continue
        redis.hset('cc:%s' % name, bug_id, 1) # Then we recreate all ids.

def save_reporter_redis(bug_id, user_id):
    name = get_user_name(user_id)
    redis.hset('my:%s' % name, bug_id, 1)

def get_html(name, bug):
    '''Returns the HTML required for the particular name.

    :param name: Name of html type
    :param bug: Bug dict.
    :return: string containing the html
    '''
    if name == 'status':
        st = bug['bug_status']
        if st == 'new':
            return '<span class="label bg-blue">%s</span>' % st
        elif st == 'open' or st == 'assigned':
            return '<span class="label bg-green">%s</span>' % st
        elif st == 'verified':
            return '<span class="label bg-orange">%s</span>' % st
        elif st == 'closed':
            return '<span class="label bg-red">%s</span>' % st
        else:
            return '<span class="label bg-grey">%s</span>' % st


def download_file(file_id):
    'Returns the filename for the given file_id.'
    return redis.hget('uploads', file_id)


def set_user_detail_redis(user):
    '''Sets the redis values for a given User object.

    :param user: model.User object
    :return: None
    '''
    redis.hset("users", user.user_name, user.id)
    redis.hset("usersid", user.id, user.user_name)


def get_my_bugs(user):
    'Finds all the bugs related to the user.'
    rids = redis.hgetall('my:%s' % user)
    reporter = []
    for bid in rids.keys():
        reporter.append(get_bug(bid))
    ccids = redis.hgetall('cc:%s' % user)
    ccs = []
    for bid in ccids.keys():
        ccs.append(get_bug(bid))
    assids = redis.hgetall('ass:%s' % user)
    ass = []
    for bid in assids.keys():
        ass.append(get_bug(bid))
    return reporter, ccs, ass
