Installation
==================================

Requirements
-------------

* flask
* flask-admin
* sqlalchemy
* python-redis
* redis
* albemic


Setting up a devel instance
---------------------------


Start the redis server
----------------------
::

		# service redis start

Database creation
-------------------
::

		$ python createdb.py

The above command will create the database schema as required.


Start the server
-----------------
::

		$ python runserver.py


Create the first user and test data
------------------------------------

Modify testdata.py file for your username, email and password.


You need to download `comps.json <https://kushal.fedorapeople.org/comps.json.tar.gz>`_
for the components.

Then run the file as::

	$ python testdata.py


This will add a user and a product and test components for the product.
