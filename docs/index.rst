.. Bugspad2 documentation master file, created by
   sphinx-quickstart on Wed Jul 30 16:07:49 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Bugspad2's documentation!
====================================

Contents:

.. toctree::
   :maxdepth: 2

   install
   design


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
