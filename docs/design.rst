Design notes
=================

We are using redis to store all search related indexes on memory. This means any
search term must be indexed in redis.

View a bug
----------

Only comments will come from db, rest should be coming from redis.